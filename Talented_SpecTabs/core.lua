local Talented = Talented
local L = LibStub("AceLocale-3.0"):GetLocale("Talented")

local specs = {}

for i = 1, 10 do 
	specs["spec" .. i] = {
		talentGroup = i,
		tooltip = TALENT_TAB_NAME:format(i),
		cache = {}}
end

specs["petspec1"] = {
	pet = true,
	tooltip = TALENT_SPEC_PET_PRIMARY,
	cache = {}}

local function UpdateSpecInfo(info)
	local pet, talentGroup = info.pet, info.talentGroup
	local tabs = GetNumTalentTabs(nil, pet)
	if tabs == 0 then return end

	local imax, min, max, total = 0, 0, 0, 0
	for i = 1, tabs do
		local cache = info.cache[i]
		if not cache then
			cache = {}
			info.cache[i] = cache
		end
		local name, icon, points = GetTalentTabInfo(i, nil, pet, (talentGroup == C_Talent.GetActiveTalentGroup() or pet) and 1 or 2 )
		points = pet and points or C_Talent.GetSpecInfoCache().talentGroupData[talentGroup][i]
		cache.name, cache.icon, cache.points = name, icon, points
		if points < min then
			min = points
		end
		if points > max then
			imax, max = i, points
		end
		total = total + points
	end
	info.primary = nil
	if tabs > 2 then
		local middle = total - min - max
		if 3 * (middle - min) < 2 * (max - min) then
			info.primary = imax
		end
	end
	return info
end

local function TabFrame_OnEnter(self)
	local info = specs[self.type]
	GameTooltip:SetOwner(self, "ANCHOR_RIGHT")
	GameTooltip:AddLine(info.tooltip)
	for index, cache in ipairs(info.cache) do
		local color = info.primary == index and GREEN_FONT_COLOR or HIGHLIGHT_FONT_COLOR
		GameTooltip:AddDoubleLine(cache.name, cache.points,
			HIGHLIGHT_FONT_COLOR.r, HIGHLIGHT_FONT_COLOR.g, HIGHLIGHT_FONT_COLOR.b,
			color.r, color.g, color.b, 1)
	end
	if not info.pet and not self:GetChecked() then
		GameTooltip:AddLine(L["Right-click to activate this spec"], GREEN_FONT_COLOR.r, GREEN_FONT_COLOR.g, GREEN_FONT_COLOR.b, 1)
	end
	GameTooltip:Show()
end

local function TabFrame_OnLeave(self)
	GameTooltip:Hide()
end

local function Tabs_UpdateCheck(self, template)
	if not template or not Talented.alternates then return end
	self.petspec1:SetChecked(template == Talented.pet_current)
	for i = 1, C_Talent.GetNumTalentGroups() do
		self["spec" .. i]:SetChecked(template == Talented.alternates[i])
	end
end

local function TabFrame_OnClick(self, button)
	local info = specs[self.type]
	if button == "RightButton" then
		if not info.pet and not InCombatLockdown() then
			C_Talent.SetActiveTalentGroup(info.talentGroup)
			Tabs_UpdateCheck(self:GetParent(), Talented.alternates[info.talentGroup])
		end
	else
		local template
		if info.pet then
			template = Talented.pet_current
		else
			template = Talented.alternates[info.talentGroup]
		end
		if template then Talented:OpenTemplate(template) end
		Tabs_UpdateCheck(self:GetParent(), template)
	end
	C_Talent.SelectTalentGroup(info.talentGroup)
	self:GetParent():GetParent().CurrencySelectFrame:SetPoint("LEFT", self, "LEFT")
	if info.talentGroup and (info.talentGroup > 2) then
		self:GetParent():GetParent().CurrencySelectFrame:Show()
	else
		self:GetParent():GetParent().CurrencySelectFrame:Hide()
	end
end

local function TabFrame_Update(self)
	local info = UpdateSpecInfo(specs[self.type])
	if info then
		self.texture:SetTexture(info.cache[info.primary or 1].icon)
	end
	for i = 1, 2 do
        local currencyInfo = C_Talent.GetCurrencyInfo(i)

        if currencyInfo then
            local currencyButton = _G["PlayerTalentFrameCurrencySelectFrameCurrency"..i]

            currencyButton.Icon:SetDesaturated(not currencyInfo.count or currencyInfo.count == 0)
            currencyButton.Count:SetText(currencyInfo.count > 99 and "99.." or currencyInfo.count)
            currencyButton.name = currencyInfo.name

            currencyButton:SetEnabled(currencyInfo.count and currencyInfo.count > 0)

            if currencyInfo.count and currencyInfo.count > 0 then
                if not C_Talent.GetSelectedCurrency() then
                    currencyButton:Click()
                end
            else
                currencyButton:SetChecked(false)
            end
        end
    end
end

local function MakeTab(parent, type)
	local tab = CreateFrame("CheckButton", nil, parent)
	tab:SetSize(32, 32)
	local t = tab:CreateTexture(nil, "BACKGROUND")
	t:SetTexture"Interface\\SpellBook\\SpellBook-SkillLineTab"
	t:SetSize(64, 64)
	t:SetPoint("TOPLEFT", -3, 11)

	t = tab:CreateTexture()
	t:SetTexture"Interface\\Buttons\\ButtonHilight-Square"
	t:SetBlendMode"ADD"
	t:SetAllPoints()
	tab:SetHighlightTexture(t)
	t = tab:CreateTexture()
	t:SetTexture"Interface\\Buttons\\CheckButtonHilight"
	t:SetBlendMode"ADD"
	t:SetAllPoints()
	tab:SetCheckedTexture(t)
	t = tab:CreateTexture()
	t:SetAllPoints()
	tab:SetNormalTexture(t)
	tab.texture = t

	tab.type = type
	tab.Update = TabFrame_Update

	tab:SetScript("OnEnter", TabFrame_OnEnter)
	tab:SetScript("OnLeave", TabFrame_OnLeave)

	tab:RegisterForClicks("LeftButtonUp", "RightButtonUp")
	tab:SetScript("OnClick", TabFrame_OnClick)

	return tab
end

local function Tabs_Update(self)
	local anchor = self.spec1
	anchor:SetPoint"TOPLEFT"
	anchor:Update()
	
	for i = 2, 10 do
		if i <= C_Talent.GetNumTalentGroups() then
			self["spec" .. i]:Show()
			self["spec" .. i]:SetPoint("TOP", anchor, "BOTTOM", 0, -20)
			self["spec" .. i]:Update()
			anchor = self["spec" .. i]
		else
			self["spec" .. i]:Hide()
		end
	end
	-- local _, pet = HasPetUI()
	local pet = UnitExists"pet"
	if pet then
		local petspec1 = self.petspec1
		petspec1:Show()
		petspec1:Update()
		petspec1:SetPoint("TOP", anchor, "BOTTOM", 0, -20)
	else
		self.petspec1:Hide()
	end
end

local function Tabs_OnEvent(self, event, ...)
	if event ~= "UNIT_PET" or (...) == "player" then
		Tabs_Update(self)
	end
end

local function MakeTabs(parent)
	local f = CreateFrame("Frame", nil, parent)
	
	for i = 1,10 do
		f["spec" .. i] = MakeTab(f, "spec" .. i)
	end
	f.petspec1 = MakeTab(f, "petspec1")

	f:SetPoint("TOPLEFT", parent, "TOPRIGHT", -2, -40)
	f:SetSize(32, 150)

	f:SetScript("OnEvent", Tabs_OnEvent)

	f:RegisterEvent"UNIT_PET"
	f:RegisterEvent"PLAYER_LEVEL_UP"
	f:RegisterEvent"PLAYER_TALENT_UPDATE"
	f:RegisterEvent"PET_TALENT_UPDATE"
	f:RegisterEvent"ACTIVE_TALENT_GROUP_CHANGED"

	f.Update = Tabs_Update
	f.UpdateCheck = Tabs_UpdateCheck
	Talented.tabs = f
	f:Update()

	local baseView = parent.view
	local prev = baseView.SetTemplate
	baseView.SetTemplate = function (self, template, ...)
		Talented.tabs:UpdateCheck(template)
		return prev (self, template, ...)
	end
	f:UpdateCheck(baseView.template)
	MakeTabs = nil
	return f
end

if Talented.base then
	MakeTabs(Talented.base)
else
	local prev = Talented.CreateBaseFrame
	Talented.CreateBaseFrame = function (self)
		local base = prev(self)
		MakeTabs(base)
		return base
	end
end
